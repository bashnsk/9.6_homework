provider "vkcs" {
    username = var.VK_CLOUD_USERNAME
    password = var.VK_CLOUD_PASSWORD
    project_id = var.VK_CLOUD_PROJECT_ID
    region = var.VK_CLOUD_REGION_NAME
    auth_url = var.VK_CLOUD_AUTH_URL
}